﻿using System;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement.Dto;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement.Services
{
    public interface IPartnerPromoCodeLimitManager
    {
        Task SetPartnerPromoCodeLimitAsync(Guid id, SetPartnerPromoCodeLimitRequest dto);
    }
}